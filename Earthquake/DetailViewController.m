//
//  DetailViewController.m
//  Earthquake
//
//  Created by Favio on 1/12/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import "DetailViewController.h"
#import <MapKit/MapKit.h>

#import "Earthquake.h"
#import "Annotation.h"

static NSDateFormatter *dateFormatter;


@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet MKMapView *earthquakeMapView;
@property (strong, nonatomic) Earthquake *earthquake;

@property (weak, nonatomic) IBOutlet UILabel *magnitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;



@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    __weak DetailViewController *weakSelf = self;
    
    [Earthquake getEarthquakeDetailWithId:[self earthquakeId] withCompletionBlock:^(NSError *error, Earthquake *earthquake) {
        
        if (!error)
        {
            weakSelf.earthquake = earthquake;
            
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
                [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
                
            });
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.magnitudeLabel setText:[NSString stringWithFormat:@"Magnitude : %.1f",weakSelf.earthquake.magnitude]];
                [weakSelf.locationLabel setText:[NSString stringWithFormat:@"Latitude: %f Longitude : %f Depth : %f ",weakSelf.earthquake.coordinate.latitude, weakSelf.earthquake.coordinate.longitude,weakSelf.earthquake.altitude]];
                [weakSelf.dateTimeLabel setText:[dateFormatter stringFromDate:[[weakSelf earthquake] date]]];
                
                
                Annotation *annotation = [[Annotation alloc] initWithCoordinate:weakSelf.earthquake.coordinate];
                [[weakSelf earthquakeMapView] addAnnotation:annotation];
                [[weakSelf earthquakeMapView] setRegion:MKCoordinateRegionMakeWithDistance(weakSelf.earthquake.coordinate, 60000, 60000)];
            });
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



@end
