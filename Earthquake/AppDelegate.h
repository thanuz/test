//
//  AppDelegate.h
//  Earthquake
//
//  Created by bot on 1/12/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

