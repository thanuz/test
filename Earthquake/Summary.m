//
//  Summary.m
//  Earthquake
//
//  Created by bot on 1/12/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import "Summary.h"
#import "ApiClient.h"
#import "Earthquake.h"

static NSString * const kSummaryMetadaKey = @"metadata";
static NSString * const kSummaryFeaturesKey = @"features";
static NSString * const kSummaryMetadataTitleKey = @"title";
static NSString * const kDetailEarthquakeSegue = @"kDetailEarthquakeSegue";


@interface Summary ()

@property (nonatomic, strong, readwrite) NSArray *earthquakes;
@property (nonatomic, copy, readwrite) NSString *title;

@end

@implementation Summary

- (instancetype)initWithDictionary:(NSDictionary *)summaryDictionary
{
    if (self = [super init])
    {
        _title = [[summaryDictionary objectForKey:kSummaryMetadaKey] objectForKey:kSummaryMetadataTitleKey];
        
        NSArray * features = [summaryDictionary objectForKey:kSummaryFeaturesKey];
        
        if ([features count]>0)
        {
            NSMutableArray *tmpArray = [NSMutableArray array];
            
            for (NSDictionary *earthquakeDict in features)
            {
                Earthquake *earthquake = [[Earthquake alloc] initWithDictionary:earthquakeDict];
                [tmpArray addObject:earthquake];
            }
            
            _earthquakes = [NSArray arrayWithArray:tmpArray];
        }
    }
    
    return self;
}

+ (NSURLSessionDataTask *)getSummaryWithCompletionBlock:(void (^)(NSError *, Summary *))block
{

    return [[ApiClient sharedClient] GET:@"summary/all_hour.geojson" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
//        NSLog(@"%@",task.response);
        if (block)
        {
            Summary *summary = [[Summary alloc] initWithDictionary:responseObject];
            block(nil, summary);
        }
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {

        if (block)
        {
            block(error,nil);
        }
    }];
    
}

@end
