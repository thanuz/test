//
//  Summary.h
//  Earthquake
//
//  Created by bot on 1/12/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Summary : NSObject

@property (nonatomic, strong, readonly) NSArray *earthquakes;
@property (nonatomic, copy, readonly) NSString *title;

- (instancetype)initWithDictionary:(NSDictionary *)summaryDictionary;

+ (NSURLSessionDataTask *)getSummaryWithCompletionBlock:(void (^)(NSError *error, Summary *summary))block;

@end
