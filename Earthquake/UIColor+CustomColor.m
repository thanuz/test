//
//  UIColor+CustomColor.m
//  Earthquake
//
//  Created by bot on 1/12/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import "UIColor+CustomColor.h"

@implementation UIColor (CustomColor)

+ (UIColor *)e_colorWithMagnitude:(CGFloat)magnitude
{
    if (0<=magnitude<=0.9)
    {
        return [UIColor colorWithRed:0.f green:1.0f blue:0.0f alpha:1.0f];
    }else if (magnitude>=9.0)
    {
        return [UIColor colorWithRed:1.0f green:0.0f blue:0.0f alpha:1.0f];
    }else
    {
        CGFloat red = (1.0f*magnitude)/10.0f;
        CGFloat blue  = 0.0f;
        CGFloat green = (1.0f*(10.0f-magnitude))/10.0f;
        return [UIColor colorWithRed:red green:green blue:blue alpha:1.0f];
    }
}
@end
