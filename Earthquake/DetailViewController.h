//
//  DetailViewController.h
//  Earthquake
//
//  Created by Favio on 1/12/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DetailViewController : UIViewController

@property (copy, nonatomic) NSString *earthquakeId;

@end
