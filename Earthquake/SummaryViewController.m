//
//  SummaryViewController.m
//  Earthquake
//
//  Created by bot on 1/12/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import "SummaryViewController.h"
#import "Earthquake.h"
#import "Summary.h"
#import "UIColor+CustomColor.h"
#import "DetailViewController.h"

static NSString * const kSummaryViewCellIdentifier = @"kSummaryViewCellIdentifier";
static NSString * const kDetailEarthquakeSegue = @"kDetailEarthquakeSegue";

@interface SummaryViewController ()

@property (strong, nonatomic) Summary *summary;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *refreshBarButton;
@property (strong, nonatomic) UIBarButtonItem *spinnerBarItem;
@property (strong, nonatomic) UILabel *customTitle;

- (void)showLoadingSpinner;
- (void)hideLoadingSpinner;
- (void)getLatestData;

- (IBAction)reloadData:(id)sender;
- (void)refreshData:(id)sender;

@end

@implementation SummaryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.customTitle = [[UILabel alloc] initWithFrame:CGRectOffset(self.navigationController.navigationBar.bounds, 40, 10)];
    [self.customTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:20.f]];
    self.customTitle.numberOfLines = 1;
    self.customTitle.minimumScaleFactor = 0.7f;
    self.customTitle.adjustsFontSizeToFitWidth = YES;
    self.customTitle.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView = self.customTitle;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = self.navigationController.navigationBar.tintColor;
    [self.refreshControl addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventValueChanged];
    [self getLatestData];
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectZero];
    [footerView setBackgroundColor:[UIColor whiteColor]];
    [[self tableView] setTableFooterView:footerView];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[self tableView] deselectRowAtIndexPath:[[self tableView] indexPathForSelectedRow] animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:kDetailEarthquakeSegue])
    {
        DetailViewController *detailVC = (DetailViewController *)[segue destinationViewController];
        
        Earthquake *selectedEarthquake = [[[self summary] earthquakes] objectAtIndex:[[[self tableView] indexPathForSelectedRow] row]];
        [detailVC setEarthquakeId:[selectedEarthquake earthquakeId]];
    }
}

#pragma mark - Spinner Methods

- (void)showLoadingSpinner
{
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [indicator setColor:[UIColor blueColor]];
    [indicator startAnimating];
    
    self.spinnerBarItem = [[UIBarButtonItem alloc] initWithCustomView:indicator];
    self.navigationItem.rightBarButtonItem = self.spinnerBarItem;
}

- (void)hideLoadingSpinner
{
    self.navigationItem.rightBarButtonItem = self.refreshBarButton;
}

#pragma mark - Refresh Data Methods

- (void)getLatestData
{
    __weak SummaryViewController *weakSelf = self;
    
    [self showLoadingSpinner];
    
    [Summary getSummaryWithCompletionBlock:^(NSError *error, Summary *summary) {
        [weakSelf hideLoadingSpinner];
        if (!error)
        {
            weakSelf.summary = summary;
            [[weakSelf tableView] reloadData];
            weakSelf.customTitle.text = summary.title;
        }else
        {
            weakSelf.customTitle.text = @"Error Loading Data";
        }
    }];
}

- (void)refreshData:(id)sender
{
    __weak SummaryViewController *weakSelf = self;
    
    [Summary getSummaryWithCompletionBlock:^(NSError *error, Summary *summary) {

        [[weakSelf refreshControl] endRefreshing];
        
        if (!error)
        {
            weakSelf.summary = summary;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[weakSelf tableView] reloadData];
                weakSelf.customTitle.text = summary.title;
            });
            
        }else
        {
            weakSelf.customTitle.text = @"Error Loading Data";
        }
    }];
}

- (IBAction)reloadData:(id)sender
{
    [self showLoadingSpinner];
    [self getLatestData];
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.summary.earthquakes.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Earthquake *earthquake = [[[self summary] earthquakes] objectAtIndex:[indexPath row]];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kSummaryViewCellIdentifier forIndexPath:indexPath];
    [[cell textLabel] setText:[earthquake place]];
    [[cell detailTextLabel] setText:[NSString stringWithFormat:@"Magnitude : %.1f",[earthquake magnitude]]];
    
    if (!cell.backgroundView)
    {
        UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
        cell.backgroundView = backgroundView;
    }
    
    cell.backgroundView.backgroundColor = [UIColor e_colorWithMagnitude:[earthquake magnitude]];
    return cell;
}


#pragma mrak - UITableViewDelegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0f;
}


@end