//
//  ApiClient.h
//  Earthquake
//
//  Created by bot on 1/12/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"

@interface ApiClient : AFHTTPSessionManager<NSURLSessionDataDelegate>

+ (instancetype)sharedClient;

@end
