//
//  Earthquake.h
//  Earthquake
//
//  Created by bot on 1/12/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


@interface Earthquake : NSObject

@property (copy, nonatomic, readonly) NSString *earthquakeId;
@property (copy, nonatomic, readonly) NSString *place;
@property (nonatomic, readonly) CGFloat altitude;
@property (nonatomic, readonly) CGFloat magnitude;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (strong, nonatomic, readonly) NSDate *date;
@property (copy, nonatomic, readonly) NSString *title;

- (instancetype)initWithDictionary:(NSDictionary *)earthquakeDictionary;


+ (NSURLSessionDataTask *)getEarthquakeDetailWithId:(NSString *)earthquakeId withCompletionBlock:(void (^)(NSError *error, Earthquake *earthquake))block;


@end