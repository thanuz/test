
//
//  Annotation.m
//  Earthquake
//
//  Created by Favio on 1/12/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import "Annotation.h"

@interface Annotation ()

@property (nonatomic,readwrite) CLLocationCoordinate2D coordinate;

@end

@implementation Annotation

- (instancetype)initWithCoordinate:(CLLocationCoordinate2D)coordinate
{

    if (self = [super init])
    {
        _coordinate = coordinate;
    }
    
    return self;
}

@end
