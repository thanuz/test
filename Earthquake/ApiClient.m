//
//  ApiClient.m
//  Earthquake
//
//  Created by bot on 1/12/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import "ApiClient.h"

static NSString * const kAPIBaseURLString = @"http://earthquake.usgs.gov/earthquakes/feed/v1.0/";

static ApiClient *_sharedClient;

@implementation ApiClient

+ (instancetype)sharedClient
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{

        
        _sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        _sharedClient.requestSerializer.timeoutInterval = 90.0f;
        
        _sharedClient = [[ApiClient alloc] initWithBaseURL:[NSURL URLWithString:kAPIBaseURLString]];
    });
    return _sharedClient;
}

@end