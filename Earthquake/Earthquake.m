//
//  Earthquake.m
//  Earthquake
//
//  Created by bot on 1/12/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import "Earthquake.h"
#import "ApiClient.h"

static NSString * const kEarthquakePropertiesKey = @"properties";
static NSString * const kEarthquakeIdKey = @"id";
static NSString * const kEarthquakePlaceKey = @"place";
static NSString * const kEarthquakeGeometryKey = @"geometry";
static NSString * const kEarthquakeCoordinateKey = @"coordinates";
static NSString * const kEarthquakeMagnitudeKey = @"mag";
static NSString * const kEarthquakeTimeKey = @"time";
static NSString * const kEarthquakeTitleKey = @"title";
static NSString * const kEarthquakeTzKey = @"tz";

@interface Earthquake ()

@property (copy, nonatomic, readwrite) NSString *earthquakeId;
@property (copy, nonatomic, readwrite) NSString *place;
@property (nonatomic, readwrite) CGFloat altitude;
@property (nonatomic, readwrite) CGFloat magnitude;
@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;
@property (copy, nonatomic, readwrite) NSString *detail;
@property (strong, nonatomic, readwrite) NSDate *date;
@property (copy, nonatomic, readwrite) NSString *title;

@end

@implementation Earthquake


- (instancetype)initWithDictionary:(NSDictionary *)earthquakeDictionary
{
    if (self = [super init])
    {
        _earthquakeId = [earthquakeDictionary objectForKey:kEarthquakeIdKey];
        NSDictionary *properties = [earthquakeDictionary objectForKey:kEarthquakePropertiesKey];
        
        _place = [properties objectForKey:kEarthquakePlaceKey];
        _magnitude = [[properties objectForKey:kEarthquakeMagnitudeKey] doubleValue];
                
        _date = [NSDate dateWithTimeIntervalSince1970:(([[properties objectForKey:kEarthquakeTimeKey] longLongValue])/1000)];
        _title = [properties objectForKeyedSubscript:kEarthquakeTitleKey];
        
        NSArray * locationPoints = [[earthquakeDictionary objectForKey:kEarthquakeGeometryKey] objectForKey:kEarthquakeCoordinateKey];
        
        if ([locationPoints isKindOfClass:[NSArray class]])
        {
            _coordinate = CLLocationCoordinate2DMake([[locationPoints objectAtIndex:1] doubleValue], [[locationPoints objectAtIndex:0] doubleValue]);
            _altitude = [[locationPoints objectAtIndex:2] doubleValue];
        }
        
    }
    
    return self;
}

+ (NSURLSessionDataTask *)getEarthquakeDetailWithId:(NSString *)earthquakeId withCompletionBlock:(void (^)(NSError *, Earthquake *))block
{
    NSString *path = [NSString stringWithFormat:@"detail/%@.geojson",earthquakeId];
    
    return [[ApiClient sharedClient] GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
    {
        if (block)
        {
            Earthquake *earthquake = [[Earthquake alloc] initWithDictionary:responseObject];
            block(nil, earthquake);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if (block)
        {
            block(error, nil);
        }
    }];
}


@end